﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterMovement : MonoBehaviour
{
    private Rigidbody2D playerRb;
    public float moveSpeed = 5.0f;
    public float jumpForce = 10;
    public float gravityMod;
    public bool isOnGround = true;
    private Animator playerAnimator;
    public GameObject player;
    

    // Start is called before the first frame update
    void Start()
    {
        //get physics and animator

        playerRb = GetComponent<Rigidbody2D>();
        Physics2D.gravity *= gravityMod;
        playerAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //player movment and animation

        if (Input.GetKey(KeyCode.Space) && isOnGround)
        {
            playerRb.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
            isOnGround = false;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * moveSpeed * Time.deltaTime;
            playerAnimator.SetFloat("WalkSpeed", moveSpeed);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.right * -moveSpeed * Time.deltaTime;
            playerAnimator.SetFloat("WalkSpeed", moveSpeed);
        }
        else
        {

            playerAnimator.SetFloat("WalkSpeed", 0);
        }
       
        //change scene

        if (player.transform.position.y < -6)
        {
            SceneManager.LoadScene("GameOver");
        }
        if (player.transform.position.x > 122)
        {
            SceneManager.LoadScene("HighScore");

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isOnGround = true;
    }

}
