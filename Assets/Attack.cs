﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public float Range = 1.5f;
    public GameObject ProjectilePrefab;
    public Transform Billy;
    public float fireRate;
    private float lastFired = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector3.Distance(Billy.position, transform.position);
        if (distanceToPlayer < Range && Time.time >= lastFired + fireRate)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        Instantiate(ProjectilePrefab, transform.position, ProjectilePrefab.transform.rotation);
        lastFired = Time.time;
    }



}
