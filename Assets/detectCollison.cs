﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class detectCollison : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public static int score;

    // Start is called before the first frame update
    void Start()
    {
        //set up score

        score = 0;
        scoreText.text = "Score: " + score;
        UpdateScore(0);
    }

    // Update is called once per frame
    void Update()
    {
        


    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        //add score and destory target 
        Destroy(gameObject);
        UpdateScore(100);
       
    }
    private void UpdateScore(int scoreToAdd)
    {

        score += scoreToAdd;
        scoreText.text = "Score: " + score;
    }

}

